<?php
    $gender = array(0 => 'Nam', 1 => 'Nữ');
    $faculty = array("" => "", "MAT" => "Khoa học máy tính","KDL" => "Khoa học vật liệu");
    $info = array();
    $info["name"] = "";
    $info["gender"] = "";
    $info["department"] = "";
    $info["address"] = "";
    $info["bday"] = "";

    if (isset($_POST['submit'])){
        session_start();
        $_SESSION["name"] = "";
        $_SESSION["gender"] = "";
        $_SESSION["department"] = $_POST["department"];
        $_SESSION["bday"] = "";
        $_SESSION['image'] = isset($_POST['submit']) &&($_POST['submit']);

    if (empty($_POST["name"]))
        $info["name"] = "<label class='error'>Hãy nhập tên!</label><br>";
    else
        $_SESSION["name"] = $_POST["name"];

    if (!isset($_POST['gender']) )
        $info["gender"] = "<label class='error'>Hãy chọn giới tính!</label><br>";
    else $_SESSION["gender"] = $_POST['gender'];

    if (empty($_POST["bday"]))
        $info["bday"] = "<label class='error'>Hãy chọn ngày sinh!</label><br>";
    else
        $_SESSION["bday"] = $_POST["bday"];

    if ($_SESSION["department"] === "")
        $info["department"] = "<label class='error'>Hãy chọn phân khoa!</label><br>";
    else  if ($_SESSION["name"] != ""  && $_SESSION["gender"] != "" && $_SESSION["bday"] != -1)
        header('Location: cf.php');
    }
    try{
        $conn = new PDO("mysql:host=$servername;dbname=test", $username, $password);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $tabe_sql_statement = 
            "CREATE TABLE `student` (
                `id` int(11) NOT NULL,
                `name` varchar(250) NOT NULL,
                `gender` int(1) NOT NULL,
                `faculty` char(3) NOT NULL,
                `birthday` datetime NOT NULL,
                `address` varchar(250) DEFAULT NULL,
                `avartar` text DEFAULT NULL
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8";
        $conn -> exec($tabe_sql_statement);
    
    
        $name_user = $_SESSION["name"];
        $gender_selected = $_SESSION["gender"];
        $faculty_selected = $_SESSION["faculty"];
        $dob_selected = $_SESSION["dob"];
        $address_selected = $_SESSION["address"];
    
        $sql = $conn-> prepare("INSERT INTO student (name, gender, faculty, birthday, address, avartar) VALUES
        (:name, :gender, :faculty, :birthday, :address)");
        $sql->bindParam(':name',$name_user);
        $sql->bindParam(':gender',$gender_selected);
        $sql->bindParam(':faculty',$faculty_selected);
        $sql->bindParam(':birthday',$dob_selected);
        $sql->bindParam(':address',$address_selected);
        $sql->execute();
    }catch(PDOException $e){
        echo "Connection failed: " . $e->getMessage();
    }

echo
"<head>
    <link rel='stylesheet' href='form.css'>
    <title>regist</title>
</head>
<body>
    <fieldset>
        <form method= 'post' action='success.php'>";
        echo $info["name"];
        echo $info["gender"];
        echo $info["department"];
        echo $info["address"];
        echo $info["bday"];
        echo "
            <table>
                <tr >
                    <td class ='td'><Text>  Họ và tên* </Text>                    </td>
                    <td><input type='text' id ='input' class = 'bbox' name ='name' value='";
                    echo isset($_POST['name']) ? $_POST['name'] : "";
                    echo "'></td>
                </tr>
                <tr>
                    <td class = 'td'><label>Giới tính* </label> </td>
                    <td>";
                            for ($i =0; $i< count($gender); $i++ ){
                                    echo ' <input type="radio" name="gender" value="' . $i .'"';
                                    echo ( isset($_POST['gender']) && $_POST['gender'] == $i) ? "checked " : ""; 
                            echo "/>" . $gender[$i];                                 
                            }
        echo
                    "</td>
                </tr>
                <tr>
                    <td class='td'>
                        <label>  Phân khoa* </label>    </td>
                    <td ><select class = 'bbox' name = 'department'>"; 
                        foreach ($faculty as $key => $value){
                            echo "<option";
                            echo (isset($_POST['department']) && $_POST['department'] == $key) ? " selected " : ""; 
                            echo " value='" . $key ."'>" . $value . "</option>";
                        }
                        echo "
                        </select></td>
                </tr> 
                <tr>
                    <td class='td'><label>Ngày sinh* </label></td>
                    <td><input type='date' class='bbox' name='bday' value='";
                    echo isset($_POST['bday']) ? $_POST['bday'] : "";
                    echo "'></td>
                </tr> 
                <tr >
                    <td class ='td'><label>  Địa chỉ </label>                    </td>
                    <td><input type='text' id ='input' class = 'bbox' name ='name' value='";
                    echo isset($_POST['address']) ? $_POST['address'] : "";
                    echo "'></td>
                    </tr> 
                    <tr >
                        <td class ='td'><label>Hình ảnh </label></td>
                        <td><input type='file' id ='image' name ='image' accept='image/png, image/jpeg' style='width: 220px'></td>
    
                    </tr>             
            </table>
            <button name='submit' type='submit'>Đăng ký</button>
        </form>
    </fieldset>
</body>";
